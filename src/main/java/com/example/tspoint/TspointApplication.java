package com.example.tspoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TspointApplication {

	public static void main(String[] args) {
		SpringApplication.run(TspointApplication.class, args);
	}

}
